<?php

use Faker\Generator as Faker;

$factory->define(App\Guest::class, function (Faker $faker) {
    return [
        'name'=> $faker->name,
        'rsvp'=> $faker->randomElement($array = array (0,1,null)),
        'invite_id'=> 0
    ];
});
