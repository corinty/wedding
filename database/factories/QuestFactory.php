<?php

use Faker\Generator as Faker;

$factory->define(Quest::class, function (Faker $faker) {
    return [
        "name"=> $faker->country,
        "notes"=> $faker->text,
        "npc_id"=> 1,
        "reward"=> $faker->numberBetween($min = 20, $max = 5000),
    ];
});
