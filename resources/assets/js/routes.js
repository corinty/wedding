import VueRouter from 'vue-router';

let routes = [

    {
        path: '/',
        component: require('./views/QuestJournal')
    },
    {
        path:'/about',
        component: require('./views/About')
    },
    {
        path:'/quest-journal',
        component: require('./views/Home')
    }

]


export default new VueRouter ({
    routes,
    linkActiveClass: 'is-active',

})