import "./bootstrap";

/**
 * NPM Dependencies
 */

import Flickity from "flickity";

import "./data";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Vue components
 */

Vue.component("header-component", require("./components/HeaderComponent.vue"));
Vue.component("my-results", require("./components/MyResultsComponent.vue"));

// rsvp components
Vue.component("rsvp-popup", require("./components/RSVPComponent.vue"));
Vue.component(
  "rsvp-response",
  require("./components/RsvpResponseComponent.vue")
);

let store = {};

const app = new Vue({
  el: "#app",

  data: {
    store,
    startOpen
  }
});

const heroCarousel = new Flickity(".carousel", {
  wrapAround: true,
  autoPlay: 4000,
  fullScreen: false
});
