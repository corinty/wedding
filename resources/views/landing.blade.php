@extends('layouts.master')
@section('content')
<section class="landing-container">
    <div class="carousel">
        <img src="/images/fore-head.JPG" alt="" class="carousel__cell" style="object-position: center 58.5%;">
        <img src="images/IMG_9843.JPG" alt="" class="carousel__cell" style="object-position: center 32%;">
        <img src="images/IMG_9607.JPG" alt="" class="carousel__cell" style="object-position: center 64%;">
        <img src="images/IMG_0086.JPG" alt="" class="carousel__cell" style="object-position: center 52%;">
        <img src="images/IMG_9598.JPG" alt="" class="carousel__cell" style="object-position: center 43%;">
        <img src="images/IMG_9493.JPG" alt="" class="carousel__cell" style="object-position: center 60%;">
        <img src="images/IMG_0148.JPG" alt="" class="carousel__cell" style="object-position: center 56%;">
    </div>
    <div id="info">

        <img src="/images/branches/24.png" alt="" class="branch" id="branch1">
        <img src="/images/branches/24.png" alt="" class="branch" id="branch2">
        <!-- <img src="/images/mario-sticker.png" alt="" style="transform: scale(.5); position: absolute; left: -111px;"> -->



        <div class="info__heading">
            <h2>Please join</p>
                <h1>Corin Ty McHargue</h1>
                <h2>and</h2>
                <h1>Rachael Lynn Brown</h1>
                <h2>on Saturday, January, 26th 2019</h2>
        </div>
        <div class="info__date-time">
            <div class="date-time__wedding">
                <h2>Wedding: 4:30 pm</h2>
                <h2>Faith Baptist Church</h2>
                <br>
                <p>4350 Russell Ave N</p>
                <p>Minneapolis, MN</p>
                <p>55412</p>
                <a href="https://goo.gl/maps/BR6Dobn9z7o" target="_blank" class="button is-link">Map</a>



            </div>
            <div class="date-time__reception">
                <h2>Reception to follow wedding:</h2>
                <h2>Theodore Wirth Chalet</h2>
                <br>
                <p>1301 Theodore Wirth Parkway</p>
                <p>Golden Valley, MN</p>
                <p>55422</p>
                <a href="https://goo.gl/maps/8TSNxCBrFET2" target="_blank" class="button is-link">Map</a>


            </div>

        </div>
        <div class="info__hotels">
            <h1>Hotels</h1>
            <div class="hotel">
                <h2>Holiday Inn & Suites</h2>
                <br>
                <p>6051 Golden Hills Dr.</p>
                <p>Minneapolis, MN</p>
                <p>55416</p>
                <a href="https://goo.gl/maps/SjFzEWXw3YQ2" target="_blank" class="button is-link">Map</a>
            </div>
            <div class="hotel">
                <h2>Double Tree</h2>
                <br>
                <p>1500 Park Pl Blvd</p>
                <p>Minneapolis, MN</p>
                <p>55416</p>
                <a href="https://goo.gl/maps/scx8Fjk7A9R2" target="_blank" class="button is-link">Map</a>
            </div>
            <div class="hotel">
                <h2>Country Inn & Suites</h2>
                <br>
                <p>62550 Freeway Blvd</p>
                <p>Brooklyn Center, MN</p>
                <p>55430</p>
                <a href="https://goo.gl/maps/5m2hoMsxvcU2" target="_blank" class="button is-link">Map</a>
            </div>
        </div>

    </div>
    <div id="story">
        <div class="story__header">
            <h1>Our Love Story</h1>
        </div>
        <p>I met Corin at a place called “Training Camp” in Pardiville, Wisconsin. I had signed up for a mission trip
            to Nepal
            through an organization called, Reign Ministries and Corin was on staff with Reign. That summer Corin had a
            bum
            leg and had to use a scooter to get around which made camp difficult, but I never heard him complain once.
            I
            noticed quickly Corin was a very kind, sweet person and he loves his tech. We slowly became friends and one
            day
            near the end of our time at camp a group of us loaded into a mini van and had lunch at Culver’s because we
            could
            not stomach another PB&amp;J. This is when Corin told me later he noticed I looked “kinda cute” now to be
            fair
            I had not showered in a week (no one had showered in a week- it was not just me!) and I was wearing my
            nasty
            camp clothes with my hair in my classic “fun bun” which was such a greasy, frizzy mess I think it is sweet
            he
            thought I looked cute at all! A few days later I left for Nepal and Corin drove back home to Minneapolis.</p>
        <p>A few days into my Nepal trip I started to receive some Facebook messages from Corin. I thought that was
            nice of
            him to check in on me. He was on staff with the ministry, so I thought it was part of his job to message
            the
            staff oversees. I asked my roommate if she was getting messages from Corin and she gave me a confused look
            and
            said, “No, why would he message me?” I blushed and said, “Okay good to know…” and I started to think
            perhaps
            this guy is into me?! I would stay up late and message Corin while he ate breakfast and messaged me. When I
            got
            home he asked me to get coffee, so he could hear about my trip. The entire time I wondered if we were on a
            date.
            Part of me thought Corin just wanted to be friends but soon my questions would be answered. Corin had to
            leave
            for a wedding in Seattle but a day after our coffee “hang” he messaged me and asked me if he could take me
            on
            a date. I squealed and leaped around my kitchen. After that first official date it did not take long to
            fall
            in love and six months later Corin proposed. We are so excited to spend the rest of our lives together. We
            are
            so thankful to our family and friends who have shaped us to be the people we are today. Finally, making
            Christ
            the center of our relationship has been important to both of us. It takes work every day, but He has loved
            us
            each fiercely and given us a beautiful gift of eternal life with Him and each other.
        </p>
    </div>
    <img src="/images/IMG_9843.JPG" alt="" class="aside__picture" id="pic1">




    <div id="party">
        <h1>Meet the Wedding Party</h1>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/michaela.jpg" alt="">
            </div>
            <div class="person__bio">

                <h2>Micheala
                    <span class="person__title"> - Maid of Honor</span>
                </h2>
                <p>Micheala and I were random roommates paired together freshman year at Bethel University. I have no
                    clue how
                    to condense a decade of friendship into three sentences but to that random computer at Bethel that
                    put
                    us in that shoebox of a room- I am forever grateful.</p>
            </div>
        </div>




        <div class="person">
            <div class="person__image">
                <img src="/images/bios/kate.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Kate
                    <span class="person__title"> - Bridesmaid</span>
                </h2>
                <p>We were also roommates that later became friends. My sophomore year at school we lived together, and
                    I found
                    Kate to be one of the sweetest, kindest, loving, creative people I have ever met. If you need a
                    hug,
                    spot of tea, a good listener or a three-page long text that can move you to tears- Kate is your
                    girl.
                </p>
            </div>
        </div>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/sydney.jpg" alt="" style="object-position: top">
            </div>
            <div class="person__bio">
                <h2>Sydney
                    <span class="person__title"> - Bridesmaid</span>
                </h2>
                <p>Sydney and I met through band at Bethel. Then we found out we were both nursing majors and ended up
                    having
                    ever class together and later we were friends that became roommates! Sydney is also one of the
                    kindest
                    souls I have ever met. Seriously she is so sweet. She can make you a mean fried egg and she can
                    start
                    a dance party any place, any time. I expect an epic dance battle between Sydney and Corin at the
                    wedding.
                    Keep an eye out!</p>
            </div>
        </div>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/joann.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Joann
                    <span class="person__title"> - Bridesmaid</span>
                </h2>
                <p>Joann is also a random roommate later turned friend- not from Bethel. (Shocking I know). She
                    introduced me
                    to Avatar the Last Airbender, Eau Claire, WI and all of her friends that then became my friends.
                    She
                    is so inclusive and insightful. We could talk for hours about eggplants or millennials in the
                    workplace.
                </p>
            </div>
        </div>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/rachel.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Rachel
                    <span class="person__title"> - Personal Attendant</span>
                </h2>
                <p>Rachel was a friend of a friend turned roommate turned friend! She is such a deep thinker and makes
                    me question
                    things I had no idea I should be questioning like genetically modified organisms (GMOs) or
                    artificial
                    intelligence (AI). Rachel is also such a sweet, fun person and I am so thankful for her friendship.</p>
            </div>
        </div>



        <div class="person">
            <div class="person__image">
                <img src="/images/bios/kevin.jpg" alt="" style="object-position: top">
            </div>
            <div class="person__bio">
                <h2>Kevin
                    <span class="person__title"> - Bestman</span>
                </h2>
                <p>Kevin and I have been good friends for many years. We originally met through mutual friends but it
                    wasn’t until we kept bumping
                    into each other at Best Buy that we started to form our own friendship. This was further deepened
                    by many hours together
                    playing video games. Kevin is someone that always can bring a smile to people's faces, he is also
                    so good at taking are of
                    and loving on others. I’m so honored to have him as my bestman in my wedding!</p>
            </div>
        </div>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/paul.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Paul
                    <span class="person__title"> - Groomsmen</span>
                </h2>
                <p>Paul is my future brother in law!!! The first time I officially met him in person was on the night
                    of Rachael and I’s engagement
                    because he just flew home from being in China for a couple of years. I’m so insanely pumped to
                    continue to build our friendship
                    as we spend more time together.</p>
            </div>
        </div>


        <div class="person">
            <div class="person__image">
                <img src="/images/bios/joe.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Joe
                    <span class="person__title"> - Groomsmen</span>
                </h2>
                <p>Joe and I first met when Rachael invited me over to her house for a bonfire. I knew from the first
                    time we hung out that Joe was my kind of people. A classy gentlemen who thinks deep but loves
                    deeper. Whenever you need someone to talk to he is certainly your man. Always full of encouragement
                    and wisdom.</p>
            </div>
        </div>
        <div class="person">
            <div class="person__image">
                <img src="/images/bios/luke.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Luke
                    <span class="person__title"> - Groomsmen</span>
                </h2>
                <p>We met at the bonfire at Rachael’s house. We happened to both wear red shorts that night which meant
                    it was destiny that we would become close friends. In all seriousness I have thoroughly enjoyed
                    every time I hang out with Luke. Our conversations always range from dungeons and dragons to deeper
                    philosophical ponderings.</p>
            </div>
        </div>

        <div class="person">
            <div class="person__image">
                <img src="/images/bios/michael.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Michael
                    <span class="person__title"> - Groomsmen</span>
                </h2>
                <p>One of my favorite memories with Michael was the weekend of his golden birthday. I was part of the
                    group that was supposed to
                    distract him during the day before the surprise party. It was a day filled with board games and
                    going to Up Down. I can’t
                    think of a better way to spend the day. Michael is such a good friend to me, his company is always
                    appreciated and I can’t
                    wait to see what happens has our friendship deepens.</p>
            </div>
        </div>
        <div class="person">
            <div class="person__image">
                <img src="/images/bios/matt.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Matt
                    <span class="person__title"> - Groomsmen</span>
                </h2>
                <p>Matt is one of the few people I know that enjoys working with computers the way that I do. I’ve
                    really enjoyed finally
                    having a friend with whom I can geek out with over computers, technology, and espresso. </p>
            </div>
        </div>
        <div class="person">
            <div class="person__image">
                <img src="/images/bios/miles.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Miles
                    <span class="person__title"> - Usher</span>
                </h2>
                <p>Miles and I were a part of the same small group at church. We don’t get the chance to get together
                    all of the time but
                    when we do it’s always a good time. One of my favorite memories of us is from my 13th birthday
                    party were we stayed up late
                    and put together a wwe wrestling video and Miles’ character was ‘The Fridge’.</p>
            </div>
        </div>
        <div class="person">
            <div class="person__image">
                <img src="/images/bios/dave.jpg" alt="">
            </div>
            <div class="person__bio">
                <h2>Dave
                    <span class="person__title"> - Usher</span>
                </h2>
                <p>Dave and I met back in high school. He was a part of my small group at church and have been friends
                    ever since. Bonds were
                    formed over all night video game parties. I can’t even count the cans of mountain dew that were
                    consumed. I think one night
                    we each drank a whole 12 pack of mountain dew code red.</p>
            </div>
        </div>
    </div>

    <div id="fun-things">
        <h1>Our Favorite Places</h1>
        <div class="fun__category content">
            <h2>Resturants
                <i class="fas fa-utensils"></i>
            </h2>
            <ul>
                <li>
                    Betty Dangers
                    <p> - Tacos and such</p>
                </li>
                <li>Ichiddo Ramen</li>
                <li>Hola Arepa</li>
                <li>Hai Hai</li>
                <li>Pat's Tap</li>
                <li>Young Joni</li>
                <li>Hazel's</li>
                <li>Hi Lo Diner</li>
            </ul>

        </div>
        <div class="fun__category content">
            <h2>Coffee
                <i class="fas fa-coffee"></i>
                </h1>
                <ul>
                    <li>Spy House</li>
                    <li>Five Watt</li>
                    <li>Dog Wood</li>
                    <li>Penny's</li>
                    <li>Angry Catfish Bikes and Coffee</li>
                    <li>Corner Coffee</li>
                    <li>Mojo Coffee</li>
                </ul>
        </div>
        <div class="fun__category content">
            <h2>Places to Visit
                <i class="fas fa-map-marker-alt"></i>
            </h2>
            <ul>
                <li>Up Down</li>
                <li>Como Park/Conservatory
                    <p>- (Where we got engaged)</p>
                </li>
                <li>Lift Bridge Brewery</li>
                <li>Mall of America</li>
                <li>Surly Brewery</li>
                <li>Dangerous Man</li>
                <li>St Anthony Main/Stonearch Bridge</li>
                <li>Stillwater</li>
                <li>Thirsty Whale Bakery</li>
            </ul>
        </div>
    </div>


    <div id="registry">
        <h1>Registry</h1>
        <img src="/images/Amazon-Logo.png" alt="" class="logo__amazon">
        <a href="https://www.amazon.com/wedding/corin-mchargue-rachael-brown-minneapolis-january-2019/registry/ECDYKU8799PY"
            target="_blank" class="button is-link registry__link amazon">View Registry</a>
        <img src="/images/Target-logo.png" alt="" class="logo__target">
        <a href="https://www.target.com/gift-registry/giftgiver?registryId=7ceddcdd6f114dde909ff1dd4266d9a0&lnk=registry_custom_url"
            target="_blank" class="button is-link registry__link target">View Registry</a>
        <img src="/images/macys-logo.png" alt="" class="logo__macys">
        <a href="https://www.macys.com/wgl/registry/guest/2390494" target="_blank" class="button is-link registry__link macys">View
            Registry</a>


    </div>
    <img src="/images/IMG_9598.JPG" alt="" class="aside__picture" id="pic2">


    <footer>
        <p>Website created by TyDev.io
            <!-- <a href="http://tydev.io">TyDev.io</a> -->
            </a>
        </p>
    </footer>


    <!-- Hero footer: will stick at the bottom -->
</section>
@endsection

@section('header')
@endsection