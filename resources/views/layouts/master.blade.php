<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="../../favicon.ico">
    <title>Corin and Rachael
    </title>

    <!-- Custom styles for this template -->
    <link href="/css/app.css" rel="stylesheet"> {{-- Animate.css Library --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg"
        crossorigin="anonymous">

</head>

<body class="has-navbar-fixed-top">
    <script>
        let startOpen= {!! $rsvpOpen !!};
        </script>
    <div id="app">
        <header-component :start-open="startOpen"></header-component>
        @yield('content')



    </div>





  <script>
      const scoutId= "{{ env('ALGOLIA_APP_ID') }}";
      const scoutSearch= "{{env('ALGOLIA_SEARCH') }}";
</script>
    <script src="/js/app.js"></script>
    {{-- slideout menu script --}}
</body>

</html>