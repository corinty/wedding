<nav class="tabs is-boxed is-centered is-medium">

        <ul>
            <router-link tag="li" to="/" exact>
                <a>Home</a>
            </router-link>
            <router-link tag="li" to="/about">
                <a>About</a>
            </router-link>
            <router-link tag="li" to="/quest-journal">
                <a>Quest Journal</a>
            </router-link>
        </ul>
    
</nav>