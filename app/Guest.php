<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Guest extends Model
{
    use Searchable;
    
    protected $fillable = [
        'rsvp','message'
    ];

    public function invite(){
        return $this->belongsTo(Invite::class);
    }
}
