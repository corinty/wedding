<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invite;

class InviteController extends Controller
{
    public function missingRSVP($inviteId){
        /**
         * Given an invite id it will return the guests that still need 
         * to RSVP
         */
        // $inviteId = 5;

        $invite = Invite::find($inviteId);
        $data = $invite->guests()->where('rsvp', null)->get();
        
        if($data->isEmpty()){
            return 0;
        } else {
            return $data;
        }
      

    }
    
    public function show($id){
        return Invite::find($id)->guests()->get();
    }
}
