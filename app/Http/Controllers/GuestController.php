<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guest;

class GuestController extends Controller
{
    public function index(){
        return Guest::all();
       
    }

    public function store(){
        $rsvp = Guest::find(request()->id);

        $rsvp->update([
            'rsvp'=> request()->rsvp,
            'message'=> request()->message
        ]);

        $rsvp->save();

    return 'success';
    }
}
